"use strict";

let num1;
let num2;
let operation;

do {
    num1 = prompt("Write your number:");
    num2 = prompt("Write second number:");
    operation = prompt("Write operation:");
    
} while (
    !(num1 && !isNaN(num1) && typeof +num1 === "number") &&
    !(num2 && !isNaN(num2) && typeof +num2 === "number") &&
    operation !== "+" &&
    operation !== "-" &&
    operation !== "*" &&
    operation !== "/"
);

function calculate(firstNum, secondNum, culcOperation) {
    firstNum = num1;
    secondNum = num2;
    culcOperation = operation;

    let result;

switch (culcOperation) {
    case "+":
        result = +firstNum + +secondNum;
        break;
    case "-":
        result = firstNum - secondNum;
        break;
    case "*":
        result = firstNum * secondNum;
        break;
    case "/":
        result = firstNum / secondNum;
        break;
    default:
        alert("ERROR!");
        break;
    };  
    return result;
}
console.log(calculate());